package net.sf.okapi.simplifier.integration;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.annotation.SimplifierRulesAnnotaton;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.resource.Custom;

public class CustomEventStep extends BasePipelineStep {
	public SimplifierRulesAnnotaton rules = null;

	@Override
	public String getName() {
		return "CustomEventStep";
	}

	@Override
	public String getDescription() {
		return "CustomEventStep: For debugging and testing only.";
	}

	@Override
	protected Event handleCustom(Event event) {
		Custom c = (Custom)event.getResource();
		rules = c.getAnnotation(SimplifierRulesAnnotaton.class);
		return super.handleCustom(event);
	}
	
	public String getRules() {
		if (rules == null) return null;
		return rules.getRules();
	}
}
